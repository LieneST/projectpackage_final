package projectPackage_Final;

import java.sql.Connection;



import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class DataBase {
	
	private Connection conn = null;

	public DataBase() throws ClassNotFoundException, SQLException {
					
		Class.forName("com.mysql.cj.jdbc.Driver");
		conn = DriverManager.getConnection("jdbc:mysql://localhost/movies", "root", "");
		conn.setAutoCommit(false);
	}
	
	//Try to insert entries
	public void insertFromList(List<Film> listOfFilms) throws Exception {

		int countSuccess = 0;
		int countNotSuccess = 0;
		
		try {
		String sqlInsert = "INSERT INTO movies.MoviesTable (Id, Film, ReleaseYear, Country, Director) VALUES (?,?,?,?,?)";

		  for (Film currentMovie : listOfFilms) {
			PreparedStatement prepStatement = conn.prepareStatement(sqlInsert);
			prepStatement.setInt(1, currentMovie.getId());
			prepStatement.setString(2, currentMovie.getName());
			prepStatement.setInt(3, currentMovie.getYear());
			prepStatement.setString(4, currentMovie.getCountry());
			prepStatement.setString(5, currentMovie.getDirector());
			if (prepStatement.executeUpdate() > 0) {
				this.conn.commit();
                countSuccess++;
                System.out.println("Entries inserted!");
            }
		}
		} catch (SQLException e) {
			throw new DataBaseException("Record(s) cannot be inserted ", e);
		} finally {
			if (countSuccess >= 1 )
				System.out.println(countSuccess + " from " + listOfFilms.size() + " records inserted!");
			if (countSuccess < listOfFilms.size()) {
				countNotSuccess = listOfFilms.size() - countSuccess;
				System.out.println(countNotSuccess + " from " + listOfFilms.size() + " records not inserted!");
			}
			}
		}
	
	
		//Try to update and/or delete entries
	public void updateFromList(List<Film> listOfFilms) throws Exception {
		
		try {
			int updateCount = 0;
			int deleteCount = 0;
			
			for (Film currentMovie : listOfFilms) {
				switch (currentMovie.getDelete()) {
				case "Yes" :			
						String sqlDelete = "DELETE FROM movies.MoviesTable WHERE Id = ?";
						PreparedStatement deletePrepStatement = conn.prepareStatement(sqlDelete);
						deletePrepStatement.setInt(1, currentMovie.getId());
						if (deletePrepStatement.executeUpdate() > 0) 
							this.conn.commit();
							deleteCount++;
							System.out.println("Record(s) deleted.");
						break;
				case "No" :
						int printId = currentMovie.getId();
						String sqlUpdate = "UPDATE movies.MoviesTable SET Film = ?, ReleaseYear = ?, Country = ?, Director = ? WHERE Id = ?";
						PreparedStatement updatePrepStatement = conn.prepareStatement(sqlUpdate);
						updatePrepStatement.setInt(5, currentMovie.getId());
						updatePrepStatement.setString(1, currentMovie.getName());
						updatePrepStatement.setInt(2, currentMovie.getYear());
						updatePrepStatement.setString(3, currentMovie.getCountry());
						updatePrepStatement.setString(4, currentMovie.getDirector());
						if (updatePrepStatement.executeUpdate() > 0) {
			                this.conn.commit();
							updateCount++;
			                System.out.println("Entries updated!");
						}  else
			          System.out.println("Something wrong with record ID = " + printId + " update!");		
					  }
			}
					
					if (updateCount >= 1)
						System.out.println(updateCount + " from " + listOfFilms.size() + " records updated!");
					if (deleteCount >= 1)
						System.out.println(deleteCount + " from " + listOfFilms.size() + " records deleted!");
		}
				
					catch (SQLException e) {
					e.printStackTrace();
		}		
	}
	
	// Try to select from Database
	public void selectFromDatabase(List<Film> listOfFilms) throws Exception {

	int countSelected = 0;
	int countNotSelected = 0;

	
	String sqlSelect = "SELECT 'FILM' FROM movies.MoviesTable WHERE Id = ?";

			try {PreparedStatement selectPrepStatement = conn.prepareStatement(sqlSelect);
				ResultSet rs = selectPrepStatement.executeQuery(sqlSelect); 
					while (rs.next()) {
					      System.out.print(rs.getInt(1));
					      System.out.print(": ");
					      System.out.println(rs.getString(2));
					}
			
				if (selectPrepStatement.executeUpdate() > 0) {
					this.conn.commit();
					countSelected++;
	                System.out.println("Entries selected!");
	            
				}
			} catch (SQLException e) {
				throw new DataBaseException("Record(s) cannot be selected ", e);
			} finally {
				if (countSelected >= 1 )
					System.out.println(countSelected + " from " + listOfFilms.size() + " records selected!");
				if (countSelected < listOfFilms.size()) {
					countNotSelected = listOfFilms.size() - countSelected;
					System.out.println(countNotSelected + " from " + listOfFilms.size() + " records not selected!");
				}
				
			}
	}
	
		public void closeConnection() {
		try {
			if (conn != null)
				conn.close();
		} catch (Exception e) {
			System.err.println(e);
		}
		}
	

class DataBaseException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public DataBaseException(String text, Throwable e) {
		super(text, e);
	}
}
}
package projectPackage_Final;

import java.util.ArrayList;


import java.util.StringTokenizer;


public class CsvFilms extends FileType{
	
	private static String delimiter = ";";

	public static void setDelimiter(String delimiter) {
	}

	@Override
	public void DataToList(String content) throws Exception {
		this.listOfFilms = new ArrayList<Film>();
		String[] lines = content.split(System.lineSeparator());

		for (int i = 1; i < lines.length; i++) {
			StringTokenizer columnNames = new StringTokenizer(lines[0], delimiter);
			StringTokenizer lineValues = new StringTokenizer(lines[i], delimiter);
			
			Film film = new Film(i, content, i, content, content, content);
			this.listOfFilms.add(film);
			
            int numberOfColumns = lineValues.countTokens();
			
			for (int j = 0; j < numberOfColumns; j++) {

				switch ((String) columnNames.nextElement()) {
				case "ID":
					film.setId(Integer.parseInt((String)lineValues.nextElement()));
                    break;
				case "Name":
                	film.setName((String) lineValues.nextElement());
					break;
				case "ReleaseYear":
					film.setYear(Integer.parseInt((String)lineValues.nextElement()));
                    break;
				case "Country":
					film.setCountry((String) lineValues.nextElement());
					break;
				case "Director":
					film.setDirector((String) lineValues.nextElement());
					break;
				case "Delete":
					film.setDelete((String) lineValues.nextElement());
					break;
				}

			}
		}

	}

}

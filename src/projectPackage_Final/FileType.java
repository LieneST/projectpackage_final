package projectPackage_Final;

import java.util.List;

public abstract class FileType {

	protected List<Film> listOfFilms; 
	
	public abstract void DataToList(String content) throws Exception;
	
	public List<Film> getListOfFilms(){
		return this.listOfFilms;
	}
	
	public static FileType getInstance (String fileType) {
		
		switch (fileType) {
		case "csv" :
			return new CsvFilms();
		case "json" :
			return new JsonFilms();
		case "xml" :
			return new XmlFilms();
		default :
			return null;
		}
	}

}

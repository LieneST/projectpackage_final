package projectPackage_Final;

import java.util.Scanner;

public class Run {
	
	public static void main(String[] args) throws Exception {
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter INSERT for new file insert in data base, enter UPDATE to update data base records:");
		String userChoise = scanner.nextLine().toUpperCase();
		
		while ("INSERT".equals(userChoise) || "UPDATE".equals(userChoise)) {
		
		Input input = new Input();
		input.process();
		FileReader fileReader = new FileReader();
		fileReader.setFile(input.getFilePath());
		if(input.getFileExtension() == "csv") 
			CsvFilms.setDelimiter(";");	
        FileType fileType = FileType.getInstance(input.getFileExtension());
		fileType.DataToList(fileReader.getContent());
		
		DataBase db = new DataBase();
		switch(userChoise) {
		case "INSERT":
			db.insertFromList(fileType.getListOfFilms());
			break;
		case "UPDATE":
			db.updateFromList(fileType.getListOfFilms());
			break;
		}

		System.out.println("Enter INSERT to insert another file into data base, UPDATE to update data base records, EXIT to close the program: ");
		String userChoiseNext = scanner.nextLine().toUpperCase();
		userChoise = userChoiseNext;
		
		}
		if ("EXIT".equals(userChoise)) {
			System.out.println("Bye");			
			scanner.close();
			
		}
		
	}
}
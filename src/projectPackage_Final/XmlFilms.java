package projectPackage_Final;

import java.io.StringReader;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlFilms extends FileType {

	@Override
	public void DataToList(String content) throws Exception {
		Document xmlDocument = this.convertToXMLDocument(content);
		processXMLDocument(xmlDocument);
	}

	private Document convertToXMLDocument(String xmlString) throws Exception {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		return builder.parse(new InputSource(new StringReader(xmlString)));
	}


	private void processXMLDocument(Document xmlDocument) {
		
		Element moviesDB = xmlDocument.getDocumentElement();
		this.listOfFilms = new ArrayList<Film>();

		NodeList listOfFilmsElements = moviesDB.getElementsByTagName("Film");
		for (int i = 0; i < listOfFilmsElements.getLength(); i++) {
			Element movieElement = (Element) listOfFilmsElements.item(i);
			NodeList movieParameters = movieElement.getChildNodes();

			Film film = new Film(i, null, i, null, null, null);
			this.listOfFilms.add(film);

			for (int j = 0; j < movieParameters.getLength(); j++) {

				Node parameter = movieParameters.item(j);

				switch (parameter.getNodeName()) {
				case "Id": {
					film.setId(Integer.parseInt(parameter.getTextContent()));
					break;
				}
				case "Name": {
					film.setName(parameter.getTextContent());
					break;
				}
				case "ReleaseYear": {
					film.setYear(Integer.parseInt(parameter.getTextContent()));
					break;
				}
				case "Country": {
					film.setCountry(parameter.getTextContent());
					break;
				}
				case "Director": {
					film.setDirector(parameter.getTextContent());
					break;
				}
				case "Delete": {
					film.setDelete(parameter.getTextContent());
					break;
				}
				}
			}

		}

	}
}
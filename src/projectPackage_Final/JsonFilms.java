package projectPackage_Final;

import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;


public class JsonFilms extends FileType{
	
	@Override
	public void DataToList(String content) throws Exception {	
		this.listOfFilms = new ArrayList<Film>();
		
		JSONObject fileObject = new JSONObject(content);
		// print for test = OK
		System.out.println(fileObject);
		
		JSONArray films = fileObject.getJSONArray("Films");
		// print for test = OK
		System.out.println(films);
		
		for (int i = 0; i < films.length(); i++) {
			JSONObject filmObject = films.getJSONObject(i);
			// print for test = OK
			System.out.println(filmObject);
			int id = Integer.parseInt((String) filmObject.get("@ID")); 
			String name = filmObject.getString("Name");
			int year = filmObject.getInt("ReleaseYear");
			String country = filmObject.getString("Country");
			String director = filmObject.getString("Director");
			String delete = filmObject.getString("Delete");
			Film film = new Film(id, name, year, country, director, delete);
			this.listOfFilms.add(film);
		}			
	}
	            
}

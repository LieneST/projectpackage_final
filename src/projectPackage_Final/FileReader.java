package projectPackage_Final;

import java.io.BufferedReader;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileReader {

	
	private BufferedReader reader;

	public void setFile(String filepath) throws FileNotFoundException,IOException {
		File file = new File(filepath);
		reader = new BufferedReader(new java.io.FileReader(file));
	}

	public String getContent() throws IOException {
		
		StringBuffer content = new StringBuffer();
		String line = this.reader.readLine();
		
		while (line != null) {
			content.append(line);
			content.append(System.lineSeparator());
			line = this.reader.readLine();
		}

		return content.toString();
	}
}

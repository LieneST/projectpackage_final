package projectPackage_Final;

public class Film {
	private int id;
	private String name;
	private int year;
	private String country;
	private String director;
	private String delete;
	
	public Film(int id, String name, int year, String country, String director, String delete) {
	        this.id = id;
	        this.year = year;
	        this.name = name;
	        this.country = country;
	        this.director = director;
	        this.delete = delete;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}

	public String getDelete() {
		return delete;
	}

	public void setDelete(String delete) {
		this.delete = delete;
	}

	
	
	

}

package projectPackage_Final2;

import java.io.BufferedWriter;


import java.io.FileWriter;
import java.util.List;

public class CreateCsvFile {

	
	public void ListToFile(List<Film2> listOfFilms2) throws Exception {
	
	BufferedWriter fileWriterCSV = new BufferedWriter(
			new FileWriter("/home/user/eclipse-workspace/ExportFiles/CSVFilmsFromDB.csv"));
	
	fileWriterCSV.append("ID");
	fileWriterCSV.append(",");
	fileWriterCSV.append("Name");
	fileWriterCSV.append(",");
	fileWriterCSV.append("Year");
	fileWriterCSV.append(",");
	fileWriterCSV.append("Country");
	fileWriterCSV.append(",");
	fileWriterCSV.append("Director");
	fileWriterCSV.append("\n");

	for (Film2 filmFromDB : listOfFilms2) {
		fileWriterCSV.append(String.format("%s,%s,%s,%s,%s",filmFromDB.getId(), filmFromDB.getName(), filmFromDB.getYear(), filmFromDB.getCountry(), filmFromDB.getDirector()));
		fileWriterCSV.append("\n");
	}

	fileWriterCSV.flush();
	fileWriterCSV.close();
	System.out.println("CSV file created");
	
	
	}
	
	
	
	
	
}

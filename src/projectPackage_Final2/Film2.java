package projectPackage_Final2;

public class Film2 {

	private int id;
	private String name;
	private int year;
	private String country;
	private String director;
					
	public Film2(int id, String name, int year, String country, String director) {
		this.id = id;
		this.name = name;
		this.year = year;
		this.country = country;
		this.director = director;
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

		

		
}

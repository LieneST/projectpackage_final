package projectPackage_Final2;

import java.io.FileWriter;

import java.io.IOException;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

//public class CreateJsonFile extends FilmList2{
	public class CreateJsonFile {


		public void ListToFile(List<Film2> listOfFilms2) throws Exception {
			
			JSONArray films = new JSONArray();
			JSONObject fileObject = new JSONObject();

			for (Film2 film2 : listOfFilms2) {
				JSONObject filmObject = new JSONObject();
				filmObject.put("ID", (film2.getId()));
				filmObject.put("Name", (film2.getName()));
				filmObject.put("ReleaseYear", (film2.getYear()));
				filmObject.put("Country", (film2.getCountry()));
				filmObject.put("Director", (film2.getDirector()));

				films.put(filmObject);
			}
		
			fileObject.put("Films", films);
		

			try {
				FileWriter newJsonFile = new FileWriter("/home/user/eclipse-workspace/ExportFiles/jsonFilmsFromDB.json");
				newJsonFile.write(fileObject.toString(3));
				newJsonFile.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			System.out.println("JSON file created");
		}

		
			
}

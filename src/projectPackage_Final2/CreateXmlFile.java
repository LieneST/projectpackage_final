package projectPackage_Final2;

import java.io.File;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CreateXmlFile {
	
	
	public void ListToFile(List<Film2> listOfFilms2) throws Exception {
	        
	      try {
	        	DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

	            // root elements
	            Document doc = docBuilder.newDocument();
	            Element rootElement = doc.createElement("Films");
	            doc.appendChild(rootElement);
	            
	            for (Film2 Film2 : listOfFilms2) {
	            	Element id = doc.createElement("Id");
	            	rootElement.appendChild((id));
	            	id.setAttribute("Id", String.valueOf(Film2.getId()));
	            	
	            	Element name = doc.createElement("Name");
	            	name.appendChild(doc.createTextNode(Film2.getName()));
	            	id.appendChild(name);
	            	
	            	Element year = doc.createElement("ReleaseYear");
	            	year.appendChild(doc.createTextNode(String.valueOf(Film2.getYear())));
	            	id.appendChild(year);
	            	
	             	Element country = doc.createElement("Country");
	             	country.appendChild(doc.createTextNode(Film2.getCountry()));
	            	id.appendChild(country);
	            	
	            	Element director = doc.createElement("Director");
	            	director.appendChild(doc.createTextNode(Film2.getDirector()));
	            	id.appendChild(director);
	            }
	            
	            TransformerFactory transformerFactory = TransformerFactory.newInstance();
	            Transformer transformer = transformerFactory.newTransformer();
	            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
	            DOMSource source = new DOMSource(doc);
	            StreamResult result = new StreamResult(new File("/home/user/eclipse-workspace/ExportFiles/xmlFilmsFromDB.xml"));
	            
	            transformer.transform(source, result);

	            System.out.println("XML file created!");
	            
	            
	      } catch (ParserConfigurationException pce) {
	          pce.printStackTrace();
	      } catch (TransformerException tfe) {
	          tfe.printStackTrace();
		}
	      
	 }
} 
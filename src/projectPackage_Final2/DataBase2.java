package projectPackage_Final2;
	
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
	
public class DataBase2 {

	
	private Connection conn;

// CONNECTION WITH DATA BASE
public DataBase2() throws ClassNotFoundException, SQLException {
	Class.forName("com.mysql.cj.jdbc.Driver");
	this.conn = DriverManager.getConnection("jdbc:mysql://localhost/movies","root","");
	this.conn.setAutoCommit(false);
}


// GET ALL TABLE FROM DATA BASE
public List<Film2> selectAllFromDatabase() throws Exception {
List<Film2> listOfFilms2 = new ArrayList<Film2>();	
Statement stmt = conn.createStatement();
ResultSet rsAll = stmt.executeQuery("SELECT * FROM movies.MoviesTable");
while (rsAll.next()) {
	Film2 filmFromDB = new Film2(rsAll.getInt("Id"), rsAll.getString("Film"), rsAll.getInt("ReleaseYear")
			, rsAll.getString("Country"), rsAll.getString("Director"));
	// Print only for test
	System.out.println(rsAll.getInt("Id") + "; " + rsAll.getString("Film") + "; " + rsAll.getInt("ReleaseYear")
				+ "; " + rsAll.getString("Country") + "; " + rsAll.getString("Director"));
	listOfFilms2.add(filmFromDB);
}
return listOfFilms2;
}


// GET BY FILM FROM DATA BASE
public List<Film2> selectByFilm(String Film) throws Exception {
int countOfFilms = 0;
Film2 specificObject = new Film2(0, null, 0, null, null);
List<Film2> listOfFilms2 = new ArrayList<Film2>();			
try {  
	String sqlByFilm = "SELECT * FROM movies.MoviesTable WHERE Film like ? ";
	PreparedStatement prepStatement = conn.prepareStatement(sqlByFilm);
	prepStatement.setString(1, "%" + Film + "%");
	ResultSet rs = prepStatement.executeQuery();
	while (rs.next())   {
		specificObject = new Film2(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5));
		countOfFilms = countOfFilms + 1;
		listOfFilms2.add(specificObject);
	}
} catch (SQLException e) {
		e.printStackTrace();  
} finally {
	System.out.println("Found " + countOfFilms + " film(s)");
}
return listOfFilms2;
}
	


// GET BY DIRECTOR FROM DATA BASE
public List<Film2> selectByDirector(String Director) {
Film2 specificObject = new Film2(0, null, 0, null, null);
List<Film2> listOfFilms2 = new ArrayList<Film2>();	
int countOfFilms = 0;
try {  
	String sqlByDir = "SELECT * FROM movies.MoviesTable WHERE Director like ? ";
	PreparedStatement prepStatement = conn.prepareStatement(sqlByDir);
	prepStatement.setString(1, "%" + Director + "%");
	ResultSet rs = prepStatement.executeQuery();
	while (rs.next())   {
		specificObject = new Film2(rs.getInt(1), rs.getString(2), rs.getInt(3), rs.getString(4), rs.getString(5));
		countOfFilms = countOfFilms + 1;
		listOfFilms2.add(specificObject);
	}
} catch (SQLException e) {
		e.printStackTrace();  
} finally {
	System.out.println("Found " + countOfFilms + " film(s)");
}
return listOfFilms2;
}


public void closeConnection() {
	try {
		if (conn != null)
			conn.close();
		conn = null;
	} catch (Exception e) {
	System.err.println(e);
	}
}


}

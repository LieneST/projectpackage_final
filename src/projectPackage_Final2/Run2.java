package projectPackage_Final2;

import java.util.Scanner;

public class Run2 {

	public static void main(String[] args) throws Exception {
			
		
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter type of the file for data import (JSON, CSV or XML): ");
		String userSelectType = scanner.nextLine().toUpperCase();
		System.out.println("Enter FILM to choose by film from DB or enter DIRECTOR to choose by "
				+ "director from DB or enter ALL to retrieve full list from DB");
		String userSelectField = scanner.nextLine().toUpperCase();
		
		DataBase2 dbSelect = new DataBase2();
		
		if ("JSON".equals(userSelectType)) {
			switch (userSelectField) {
				case "FILM":
					Scanner scannerFilm = new Scanner(System.in);
					System.out.println("Enter name of the film: ");
					String userSelectFilm = scannerFilm.nextLine();					
					CreateJsonFile jsonbyFilm = new CreateJsonFile();
					jsonbyFilm.ListToFile(dbSelect.selectByFilm(userSelectFilm));
					break;
				case "DIRECTOR":
					Scanner scannerDirector = new Scanner(System.in);
					System.out.println("Enter name of the director: ");
					String userSelectDirector = scannerDirector.nextLine();					
					CreateJsonFile jsonbyDirector = new CreateJsonFile();
					jsonbyDirector.ListToFile(dbSelect.selectByDirector(userSelectDirector));
					break;
				case "ALL":			
					CreateJsonFile jsonAll = new CreateJsonFile();
					jsonAll.ListToFile(dbSelect.selectAllFromDatabase());
					break;
			}		
		}
		if ("CSV".equals(userSelectType)) {
			switch (userSelectField) {
				case "FILM":
					Scanner scannerFilm = new Scanner(System.in);
					System.out.println("Enter name of the film: ");
					String userSelectFilm = scannerFilm.nextLine();					
					CreateCsvFile jsonbyFilm = new CreateCsvFile();
					jsonbyFilm.ListToFile(dbSelect.selectByFilm(userSelectFilm));
					break;
				case "DIRECTOR":
					Scanner scannerDirector = new Scanner(System.in);
					System.out.println("Enter name of the director: ");
					String userSelectDirector = scannerDirector.nextLine();					
					CreateCsvFile jsonbyDirector = new CreateCsvFile();
					jsonbyDirector.ListToFile(dbSelect.selectByDirector(userSelectDirector));
					break;
				case "ALL":			
					CreateCsvFile jsonAll = new CreateCsvFile();
					jsonAll.ListToFile(dbSelect.selectAllFromDatabase());
					break;
			}		
		}
		if ("XML".equals(userSelectType)) {
			switch (userSelectField) {
				case "FILM":
					Scanner scannerFilm = new Scanner(System.in);
					System.out.println("Enter name of the film: ");
					String userSelectFilm = scannerFilm.nextLine();					
					CreateXmlFile xmlbyFilm = new CreateXmlFile();
					xmlbyFilm.ListToFile(dbSelect.selectByFilm(userSelectFilm));
					break;
				case "DIRECTOR":
					Scanner scannerDirector = new Scanner(System.in);
					System.out.println("Enter name of the director: ");
					String userSelectDirector = scannerDirector.nextLine();					
					CreateXmlFile xmlbyDirector = new CreateXmlFile();
					xmlbyDirector.ListToFile(dbSelect.selectByDirector(userSelectDirector));
					break;
				case "ALL":			
					CreateXmlFile xmlAll = new CreateXmlFile();
					xmlAll.ListToFile(dbSelect.selectAllFromDatabase());
					break;
			}		
		}
		
		scanner.close();
        dbSelect.closeConnection();
		
	}
}
